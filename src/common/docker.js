/**
 * Docker-related operations.
 */

const fs = require("fs");
const request = require("request-promise");
const path = require("path");
const tar = require("tar");
var Promise = require('bluebird');

const logger = require("./logger");

const docker_url = "http://unix:/var/run/docker.sock:";

/**
 * Build Docker Image by Using the `Dockerfile` from Submission
 * @param {string} submissionPath
 * @param {string} submissionId
 */
module.exports.buildDockerImage = async (submissionPath, submissionId) => {
  // BUILD DOCKER IMAGE
  logger.info(`Docker Image Creation Started for ${submissionId}`);

  let files = fs.readdirSync(path.resolve(`${submissionPath}/code`));
  const codeFolder = path.resolve(`${submissionPath}/code`);
  const dockerFile = path.resolve(`${submissionPath}/Dockerfile.tar.gz`);

  logger.info("Creating Tar ball of Dockerfile");

  tar.c(
    {
      gzip: true,
      cwd: codeFolder,
      file: dockerFile,
      sync: true
    },
    files
  )

  const headerOptions = {
    headers: {
      "Content-Type": "application/tar",
      host: null
    },
    body: fs.createReadStream(path.join(submissionPath, "Dockerfile.tar.gz")),
    encoding: null
  };

  logger.info("Calling Docker API for creating Image");
  await request
    .post(docker_url + "/build?t=" + submissionId, headerOptions)
    .then(function(res) {
      logger.info("Docker Image has been created successfully");
      return true;
    })
    .catch(function(err) {
      throw err;
    });
};

/**
 * Delete Docker Image
 * @param {string} imageName
 */
module.exports.deleteDockerImage = async imageName => {
  // BUILD DOCKER IMAGE
  logger.info(`Delete Docker Image - ${imageName}`);

  const headerOptions = {
    headers: {
      "Content-Type": "application/json",
      host: null
    }
  };

  await request
    .delete(docker_url + "/images/" + imageName, headerOptions)
    .then(function(res) {
      logger.info(`Docker Image (${imageName}) Deleted Successfully`);
      return true;
    })
    .catch(function(err) {
      throw err;
    });
};

/**
 * Create container using Tester Image and mapping the `prediction` and `ground truth` data
 * @param {string} submissionPath Path for downloaded submission on host machine
 * @param {string} submissionId Submission ID
 * @returns {string} Container ID
 */
module.exports.createContainer = async (
  submissionPath,
  submissionId,
  imageName,
  mountPath,
  testCommand
) => {
  logger.info(`Docker Container Creation Started for ${submissionId}`);

  const headerOptions = {
    headers: {
      "Content-Type": "application/json",
      host: null
    },
    json: {
      Image: imageName,
      HostConfig: {
        Binds: [eval(mountPath)],
        NetworkDisabled: true
      },
      Cmd: testCommand,
      Tty: true
    }
  };

  return await request
    .post(docker_url + "/containers/create?name=" + submissionId, headerOptions)
    .then(function(res) {
      logger.info(
        `Docker Container (${res.Id}) Creation Completed for ${submissionId}`
      );
      return res.Id;
    })
    .catch(function(err) {
      throw err;
    });
};

/**
 * Execute (Start + Run) testing process inside container to match `prediction` and `ground truth` data
 * @param {string} containerID
 * @param {[string]} testingCommand String array of testing command
 * @returns {number} Score of the submission
 */
module.exports.executeSubmission = async (
  submissionPath,
  containerID,
  testingCommand
) => {
  // START CONTAINER
  logger.info(`Starting Docker Container ${containerID}`);
  await request
    .post(docker_url + "/containers/" + containerID + "/start", {
      headers: {
        host: null
      }
    })
    .then(function(res) {
      logger.info(`Docker Container Started ${containerID}`);
      return true;
    })
    .catch(function(err) {
      throw err;
    });

  await request
    .post(docker_url + "/containers/" + containerID + "/wait", {
      headers: {
        host: null
      }
    })
    .then(function(res) {
      const statusCode = JSON.parse(res).StatusCode
      if (statusCode === 0) return true;
      else new Error(`Execution completed with error code ${statusCode}`);
    })
    .catch(function(err) {
      throw err;
    });
};

/**
 * Stop and Delete Container
 * @param {string} containerId
 */
module.exports.killContainer = async containerId => {
  const headerOptions = {
    headers: {
      host: null
    }
  };

  // DELETE CONTAINER
  logger.info(`Deleting Container ${containerId}`);
  await request
    .delete(docker_url + "/containers/" + containerId, headerOptions)
    .then(function(res) {
      logger.info(`Container Deleted ${containerId}`);
      return true;
    })
    .catch(function(err) {
      throw err;
    });
};
