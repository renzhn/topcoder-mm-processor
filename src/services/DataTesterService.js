/**
 * Tester Service for `Data` type of Marathon Matches
 */

const fs = require("fs");
const path = require("path");
const config = require("config");

const logger = require("../common/logger");

const {
  createContainer,
  executeSubmission,
  killContainer
} = require("../common/docker");

let submissionDirectory;
let containerId;

module.exports.performDataTest = async(submissionId, submissionPath) => {
  try {
    logger.debug(
      `Inside performTest of DataTester: / ${submissionId} / ${submissionPath}`
    );
    submissionDirectory = path.resolve(`${submissionPath}/submission`);

    let testCommand = [];
    testCommand = config.TESTER_COMMAND;

    // Start container
    containerId = await Promise.race([
      createContainer(
        submissionDirectory,
        submissionId,
        config.DOCKER_IMAGE_NAME,
        config.DOCKER_MOUNT_PATH,
        testCommand
      ),
      new Promise((resolve, reject) => {
        setTimeout(
          () => reject("Timeout :: Docker Container Start"),
          config.TESTING_TIMEOUT
        );
      })
    ]);

    // Execute the tester
    score = await Promise.race([
      executeSubmission(submissionDirectory, containerId, testCommand),
      new Promise((resolve, reject) => {
        setTimeout(
          () => reject("Timeout :: Docker Execution"),
          config.TESTING_TIMEOUT
        );
      })
    ]);

    logger.info("Testing cycle completed");
  } catch (error) {
    logger.error(error);
    throw new Error();
  } finally {
    await killContainer(containerId);
  }

  return;
}
