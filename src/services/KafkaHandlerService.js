/**
 * Service for Kafka handler.
 */
const _ = require("lodash");
const AWS = require("aws-sdk");
const archiver = require("archiver");
const config = require("config");
const fs = require("fs");
const ncp = require("ncp");
const path = require("path");
const { performDataTest } = require("./DataTesterService");
const { performCodeTest } = require("./CodeTesterService");
const ReviewProducerService = require("./ReviewProducerService");
const rimraf = require("rimraf");
const s3 = new AWS.S3();

const helper = require("../common/helper");
const logger = require("../common/logger");

const reviewProducer = new ReviewProducerService(config);

/**
 * Handle Kafka message.
 * @param {Object} message the Kafka message in JSON format
 */
async function handle(message) {
  logger.info(`Kafka message: ${JSON.stringify(message, null, 2)}`);

  const avScanReviewTypeId = await helper.getReviewTypeId(
    config.AV_SCAN_REVIEW_NAME
  );

  const targetedChallenge = config.CHALLENGE_ID;

  let topic = _.get(message, "topic", "");
  let resource = _.get(message, "payload.resource", "");
  let typeId = _.get(message, "payload.typeId", "");
  let reviewScore = _.get(message, "payload.score", "");
  let testPhase = _.get(message, "payload.testType", "provisional");

  console.log(resource, "/", typeId, "/", reviewScore);

  if (
    !(resource === "review" && typeId === avScanReviewTypeId) &&
    !(resource === "score")
  ) {
    logger.info(
      `Message payload resource or typeId is not of interest, the message is ignored: ${_.get(
        message,
        "payload.resource",
        ""
      )} / ${_.get(message, "payload.typeId", "")}`
    );
    return;
  }

  let submissionId = _.get(message, "payload.submissionId", "");
  if (!submissionId) {
    throw new Error("No submission id present in event. Cannot proceed.");
  }

  // Check if AV scan successful
  if (
    resource === "review" &&
    typeId === avScanReviewTypeId &&
    reviewScore !== 100
  ) {
    throw new Error(
      `AV Scan has not succeeded for submission ${submissionId}, score: ${score}`
    );
  }

  // Initiate `execution` and `error` logs
  logger.addFileTransports(
    path.join(__dirname, "../../submissions", submissionId),
    submissionId
  );

  // Get the submission by submission id
  logger.info(`Fetch submission using ${submissionId}`);
  const submission = await helper.getSubmission(submissionId);

  // Extract `challengeId from the submission object
  const challengeId = _.get(submission, "challengeId");
  logger.info(`Fetch challengeId from submission ${challengeId}`);

  // Check for expected challengeId
  if (!challengeId || challengeId !== targetedChallenge) {
    logger.info(
      `Ignoring message as challengeId - ${challengeId} is not of interest`
    );
    return;
  }

  const testConfig = JSON.parse(
    fs.readFileSync(path.join(__dirname, "../../config/phase-config.json"))
  );

  try {
    const testType = testConfig[testPhase].testType;
    logger.info(
      `Processing the submission ${submissionId} / phase: ${testPhase} / type: ${testType}`
    );

    // Download submission
    const submissionPath = await helper.downloadAndUnzipFile(submissionId);

    // Copy input data to submission folder
    const inputDataDir =
      testPhase === "final" ? "final_data" : "provisional_data";
    const inputDir = path.join(__dirname, `../../${inputDataDir}`);

    logger.info(
      `copying input directory to ${submissionPath}/submission/truth`
    );

    await new Promise((resolve, reject) => {
      ncp(inputDir, `${submissionPath}/submission/truth`, err => {
        if (err) {
          logger.error(err);
          reject(err);
        } else resolve();
      });
    });

    let score = 0;

    if (testType === "code") {
      logger.info(
        `Started executing CODE type of submission for ${submissionId} | ${submissionPath}`
      );
      await performCodeTest(submissionId, submissionPath);
    }

    logger.info(
      `Started executing DATA type of submission for ${submissionId} | ${submissionPath}`
    );
    await performDataTest(submissionId, submissionPath);

    let resultFile = fs.readFileSync(
      path.join(`${submissionPath}/submission/output`, "result.txt"),
      "utf-8"
    );

    let lines = resultFile.trim().split("\n");
    score = lines.slice(-1)[0];

    logger.info(`Create Review for ${submissionId} with Score = ${score}`);
    reviewProducer.createReview(submissionId, score, testPhase);
  } catch (error) {
    logger.error(error);
    logger.info("Create Review with Zero Score");
    await reviewProducer.createReview(submissionId, 0, testPhase);
  } finally {
    // Created zipped directory of workdir and logs
    const archive = archiver.create("zip", {});

    let filePath = path.join(__dirname, "../../submissions", submissionId);

    const output = fs.createWriteStream(
      `${filePath}/${submissionId}-${testPhase}.zip`
    );

    archive.pipe(output);
    await archive.directory(`${filePath}/output`, false);
    await archive.directory(`${filePath}/logs`, false).finalize();

    const fileName = `${submissionId}/${submissionId}-${testPhase}.zip`;
    const zipFile = fs.createReadStream(
      `${filePath}/${submissionId}-${testPhase}.zip`
    );

    const params = {
      Bucket: config.aws.S3_BUCKET,
      Key: fileName,
      Body: zipFile,
      ContentType: "application/zip",
      Metadata: {
        originalname: fileName
      }
    };

    logger.info("Upload output zip file to S3 bucket");
    await s3.upload(params).promise();

    rimraf.sync(`${filePath}/submission/code`);
    rimraf.sync(`${filePath}/submission/solution`);
    rimraf.sync(`${filePath}/submission/truth`);
    rimraf.sync(`${filePath}/logs`);
    logger.info(`Process complete for submission: ${submissionId}`);
  }
}

// Exports
module.exports = {
  handle
};

logger.buildService(module.exports);
