# tc-mm-scorer
Abstract MM Scorer, that can be used for any new MM challenges

*Follow Following Steps To Setup MM Processor For New Challenge*

## - Clone this repository

## - Generate Docker Image of `Scorer Tool`
1. Use the sample `Dockerfil` available
2. Update the steps, according to your scorer tool
3. Build image 
>`docker build -t topcoder/<IMAGE NAME>:<IMAGE_VERSION> -f Dockerfile .`
4. Tag the image as `latest`
>`docker tag topcoder/<IMAGE NAME>:<IMAGE_VERSION> topcoder/<IMAGE NAME>:latest`
5. Push docker image to `topcoder` dockerhub account
>`docker push topcoder/<IMAGE NAME>:latest`

## - Wire Up Scorer with TC Platform
1. Update following `env variables` in `ecosystem.config.js` file as per new challenge:
    * DOCKER_IMAGE_NAME
    * EXECUTION_COMMAND
    * CHALLENGE_ID
    * REVIEW_TYPE_NAME
    * REVIEWER_ID_NAMESPACE

## - Start MM Processor
- Install `pm2`
>`sudo npm install pm2 -g`
- Install node deps
>`npm install`
- Start processor
>`pm2 start`